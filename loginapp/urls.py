from django.urls import path
from . import views
from .views import user_login, user_logout,success


urlpatterns = [
    path ('', user_login, name="user_login"),
    path('success/', success, name= "user_success"),
    path('logout/', user_logout, name="user_logout"),
  
]