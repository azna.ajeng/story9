from django.test import TestCase,Client,LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
import time

class story9(TestCase) :
    def test_ada_url_halaman_login(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def test_html_login(self):
        response = Client().get('')
        self.assertTemplateUsed(response,'index.html')

    def test_button_login(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content) 
        self.assertIn("Login", content)
    
    def test_ada_url_halaman_logout(self):
        response = Client().get('/success/')
        self.assertEqual(response.status_code, 200)

    def test_html_logout(self):
        response = Client().get('/success/')
        self.assertTemplateUsed(response,'success.html')

    def test_html_success(self):
        response = Client().get('/success/')
        self.assertTemplateUsed(response, 'success.html')


# class FunctionalTest(unittest.TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.browser.implicitly_wait(25)
#         super(FunctionalTest,self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(FunctionalTest, self).tearDown()

    
#     def test_login_logout_showing_message(self):
#         self.browser.get('http://127.0.0.1:8000/')

#         inputusername = self.browser.find_element_by_id("username")
#         inputpass = self.browser.find_element_by_id("password")
#         loginbtn = self.browser.find_element_by_id("login")

#         inputusername.send_keys("azna")
#         inputpass.send_keys("aznaajeng")
#         loginbtn.click()
#         time.sleep(2)

#         self.assertEqual(self.browser.current_url, "http://127.0.0.1:8000/success/")
#         # time.sleep(2)

#         logoutbtn = self.browser.find_element_by_id("logout")
#         logoutbtn.click()
#         time.sleep(2)
#         self.assertEqual(self.browser.current_url, "http://127.0.0.1:8000/")
#         time.sleep(5)
